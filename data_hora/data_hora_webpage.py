from datetime import datetime
import os.path
import urllib2


data_atual = datetime.now()
data_padronizada = data_atual.strftime('%d/%m/%Y %H:%M')


class DataHora:
    def escrever_data_hora(self):
        caminho = os.path.join('arquivos', 'data_hora.txt')
        with open(caminho, 'a') as arquivo:
            arquivo.write(data_padronizada)
            arquivo.write("\n")

    def data_hora(self):
        if os.path.exists('arquivos/data_hora.txt'):
            self.escrever_data_hora()
            print('Data e hora registradas.')
        else:
            self.escrever_data_hora()
            print('Arquivo criado.')

    def verificar_ou_criar_dir(self):
        if os.path.isdir('arquivos'):
            print('Diretorio pronto para uso.')
        else:
            os.mkdir('arquivos')
            print('Diretorio criado.')
            self.data_hora()


class WebPage:
    def webpage(self):
        response = urllib2.urlopen("http://www.google.com.br")
        local = os.path.join('arquivos', 'google.html')
        with open(local, 'a') as arq:
            arq.write(response.read())

        print('Arquivo html no diretorio.')










