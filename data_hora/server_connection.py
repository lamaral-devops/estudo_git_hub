import paramiko
from scp import SCPClient
import os
import socket


class SSH(object):
    ssh = None
    scp = None
    senhaServidor = os.environ['SENHA_SERVIDOR_INFODEVOPS2']
    usuario = None
    servidor = None
    chaveAcesso = None

    def __init__(self, servidor, usuario):
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.usuario = usuario
        self.servidor = servidor

    def senha(self, senhaServidor):
        self.senhaServidor = senhaServidor

    def chave_acesso(self, chaveAcesso):
        self.chave_acesso = chaveAcesso

    def abrir_conexao(self):
        if self.senhaServidor is not None:
            self.ssh.connect(self.servidor, username=self.usuario, password=self.senhaServidor)
        else:
            self.ssh.connect(self.servidor, username=self.usuario, key_filename=self.chaveAcesso)

        self.scp = SCPClient(self.ssh.get_transport())
        print("Conexao aberta: %s@%s" % (self.usuario, self.servidor))

    def pegar_hostname(self):
        hostname = socket.gethostbyname('infodevops2')
        name = socket.gethostbyaddr(hostname)
        print "HOSTNAME:", name[0]

    def fechar_conexao(self):
        self.ssh.close()
        self.scp.close()
        print("Conexao fechada.")